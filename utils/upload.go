package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/imroc/req"
	"io/ioutil"
	"net/url"
	"os"
	"regexp"
)

type Body struct {
	Text string `json:"text"`
}

type AuthJson struct {
	AuthDomain string `json:"authdomain"`
	AuthPassword string `json:"authpassword"`
}

func GetSid(siteUrl string) (sid string, err error) {
	r, err := req.Post(fmt.Sprintf("%s/admin/login", siteUrl))
	if err != nil {
		return
	}
	cookies := r.Response().Cookies()
	for _, cookie := range cookies {
		if cookie.Name == "connect.sid" {
			sid = cookie.Value
			return
		}
	}
	err = errors.New("no sid in cookies")
	return
}


func AuthSid(siteUrl, sid, pass string) (err error) {
	header := req.Header{
		"Accept":       "*/*",
		"Content-Type": "application/json",
		"Cookie": 		fmt.Sprintf("connect.sid=%s; passcode=",sid),
	}

	u, err := url.Parse(siteUrl)
	if err != nil {
		return
	}

	body := AuthJson{
		AuthDomain:   u.Host,
		AuthPassword: pass,
	}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		return
	}

	r, err := req.Post(fmt.Sprintf("%s/auth/login", siteUrl), header, bodyJson)

	//fmt.Println(r.Request())
	//fmt.Println(r.Response())

	if err != nil {
		return
	}

	if r.Response().StatusCode != 200 {
		err = errors.New("not 200")
	}
	return

}

func SendFile(body []byte, password, sid, siteUrl, filename string) (err error){
	header := req.Header{
		"Accept":       "*/*",
		"Content-Type": "application/json",
		"Secret":       password,
		"Origin":       siteUrl,
		"Referer":      fmt.Sprintf("%s/edit/%s", siteUrl, filename),
		"Cookie":       fmt.Sprintf("connect.sid=%s; passcode=%s",sid, password),
		"user-agent":   "wisenuploader",
	}
	r, err := req.Post(fmt.Sprintf("%s/save", siteUrl), header, body)
	//fmt.Println(r.Request())
	//fmt.Println(r.Response())
	if (err == nil) && r.Response().StatusCode != 200 {
		err = errors.New("not 200 Response")
	}
	return
}

func ReadFile(filename string) (bytes []byte, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	bytes, err = ioutil.ReadAll(file)

	content := string(bytes)
	body := Body{Text: content}

	bytes, err = json.Marshal(body)
	return
}

func GetFilesFromDir(dir string) (res []string, err error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}
	for _, file := range files {
		reg := regexp.MustCompile(`.*\.(html|css|js)`)
		if reg.MatchString(file.Name()) {
			res = append(res, file.Name())
		}
	}
	return
}








