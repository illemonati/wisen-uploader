package main

import (
	"fmt"
	"gopkg.in/alecthomas/kingpin.v2"
	"log"
	"sync"
	"wisen-uploader/utils"
)

var (
	dir 		= kingpin.Flag("dir", "dir to find files").Short('d').Default("./").String()
	baseUrl		= kingpin.Flag("url", "base url for wisen page").Short('u').Required().String()
	password	= kingpin.Flag("pass", "password to wisen page").Short('p').Required().String()
)

func main() {
	kingpin.Parse()
	log.Printf("Uploading files from %s to %s\n", *dir, *baseUrl)
	sid, err := utils.GetSid(*baseUrl)
	if err != nil {
		log.Fatal(err)
		return
	}
	err = utils.AuthSid(*baseUrl, sid, *password)
	if err != nil {
		log.Fatal(err)
		return
	}
	files, err := utils.GetFilesFromDir(*dir)
	if err != nil {
		log.Fatal(err)
		return
	}
	wg := new(sync.WaitGroup)
	for i, filename := range files {
		wg.Add(1)
		go func(i int, filename string) {
			file, err := utils.ReadFile(fmt.Sprintf("%s/%s", *dir, filename))
			if err != nil {
				log.Printf("%d : %s : %s", i, filename, err)
				wg.Done()
				return
			}
			err = utils.SendFile(file, *password, sid, *baseUrl, filename)
			if err != nil {
				log.Printf("%d : %s : %s", i, filename, err)
				wg.Done()
				return
			}
			log.Printf("%d : %s : %s", i, filename, "success")
			wg.Done()
		}(i, filename)
	}
	wg.Wait()
}
