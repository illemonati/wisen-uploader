# Thing to upload to wisen space

```
usage: wisen-uploader --url=URL --pass=PASS [<flags>]

Flags:
      --help       Show context-sensitive help (also try --help-long and --help-man).
  -d, --dir="./"   dir to find files
  -u, --url=URL    base url for wisen page
  -p, --pass=PASS  password to wisen page

```

run make to make

the purpose of this tool is to facilitate the use of other ides with wisen space websites

it will upload all top-level css, html, and js files onto the desired wisen space site

basically any top-level files with names matching ```.*\.(html|css|js)``` will be uploaded

Steps:

1. get a auth cookie

2. login with the cookie and password

3. find all top level html | css | js files

4. upload everything in parallel

5. ...

6. profit